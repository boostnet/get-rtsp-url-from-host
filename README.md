# Get RTSP URL from host

---

### Описание

Получить адрес RTSP потока по Хосту.

### Запуск приложения в dev-режиме

- Клонировать репозиторий.

```bash

git@gitlab.com:boostnet/get-rtsp-url-from-host.git

```

- Установить и активировать виртуальное окружение c учетом версии Python 3.10

```bash

python3 -m venv venv

source venv/bin/activate

python -m pip install --upgrade pip

pip install -r requirements.txt

```

### Аргументы коммандной строки

```bash
  -h, --help            show this help message and exit
  --server SERVER, -s SERVER Адрес хоста

  --password PASSWORD, -p PASSWORD Пароль

  --username USERNAME, -u USERNAME Имя пользователя

  --local, -l           Поиск в локальнй сети
```

### Запуск приложения

```bash
python cam.py --server 109.74.133.138 --username {username} --password {password}

```
