import logging
from logging.handlers import RotatingFileHandler
from urllib.parse import urlparse

import cv2 as cv
from onvif import ONVIFCamera

from scan import PortScaner

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

file_log_handler = RotatingFileHandler(
    filename='main.log',
    encoding='utf8',
    maxBytes=50000000,
    backupCount=5
)
console_out_handler = logging.StreamHandler()
formatter = logging.Formatter(
    '%(asctime)s [%(levelname)s] %(message)s'
)
file_log_handler.setFormatter(formatter)
logger.addHandler(file_log_handler)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CamStreamUrl:
    def __init__(self, host, port, password, user, local=False):
        self.host = host
        self.port = port
        self.password = password
        self.user = user
        self.local = local
        self.auth_host = f'{user}:{password}@{host}'
        self.result = []
        self.wsdl_dir = "wsdl/"
        self.get_url_from_onvif()
        self.main()

    def get_url_from_onvif(self):
        logger.debug(f'Опрашиваем Ovif по адресу: {self.host}')
        self.cam = ONVIFCamera(self.host, self.port,
                               self.user, self.password, self.wsdl_dir)
        self.device_service = urlparse(self.cam.devicemgmt.GetServices(
            {'IncludeCapability': True})[0].XAddr)

        # Костыль
        if self.host != self.device_service.hostname and not self.local:
            logger.debug('Используем не локальный адрес!')
            for key in self.cam.xaddrs:
                self.cam.xaddrs[key] = self.cam.xaddrs[key].replace(
                    self.device_service.hostname, self.host)

        self.media_service = self.cam.create_media_service()

        profiles = self.media_service.GetProfiles()
        self.url = self.media_service.create_type('GetStreamUri')
        self.url.ProfileToken = profiles[0].token
        self.url.StreamSetup = {
            'Stream': 'RTP_unicast',
            'Transport': {
                'Protocol': 'RTSP'
            }
        }
        self.stream_url = self.media_service.GetStreamUri(self.url).Uri
        self.stream_url = self.stream_url.replace(
            self.device_service.hostname, self.auth_host)
        if not self.local:
            logger.debug('Заменяем имя хоста в адресе потока!')
            self.stream_url = self.stream_url.replace(
                self.device_service.hostname, self.host)
            self.stream_url

    def test_stream(self, stream_url):
        logger.debug(f'Проверяем поток: {stream_url}')
        cap = cv.VideoCapture(stream_url)
        if not cap.isOpened():
            logger.error('Не могу открыть поток')
            return
        else:
            logger.debug('Поток удалось получить!')
            self.result.append(stream_url)
            cap.release()

    def main(self):
        stream_port = str(urlparse(self.stream_url).port)
        stream_url = self.stream_url
        if not self.test_stream(stream_url):
            scan = PortScaner(self.host)
            for port in scan.open_ports:
                new_stream_url = self.stream_url.replace(
                    stream_port, str(port))
                self.test_stream(new_stream_url)
        else:
            self.result.append(self.stream_url)
