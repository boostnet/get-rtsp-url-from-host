import tkinter as tk
from threading import Thread
from tkinter import ttk

from cam import CamStreamUrl
from video import VideoWindow


class AsyncRun(Thread):
    def __init__(self, server, login, password, local):
        super().__init__()
        self.url_list = []
        self.server = server
        self.local = bool(local)
        self.login = login
        self.password = password

    def run(self):
        onvif = CamStreamUrl(self.server, 80, self.password,
                             self.login, self.local)
        self.url_list = onvif.result


class MainFrame(ttk.Frame):
    def __init__(self, container):
        super().__init__(container)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(0, weight=3)

        self.__create_widgets()

    def __create_widgets(self):
        self.server_var = tk.StringVar()
        ttk.Label(self, text='Адрес сервера:').grid(
            column=0, row=0, sticky=tk.W)
        server_entry = ttk.Entry(
            self, width=30, textvariable=self.server_var)
        server_entry.focus()
        server_entry.grid(column=1, row=0, sticky=tk.W)

        self.login_var = tk.StringVar()
        ttk.Label(self, text='Имя пользователя:').grid(
            column=0, row=1, sticky=tk.W)
        login_entry = ttk.Entry(self, width=30, textvariable=self.login_var)
        login_entry.focus()
        login_entry.grid(column=1, row=1, sticky=tk.W)

        self.password_var = tk.StringVar()
        ttk.Label(self, text='Пароль:').grid(
            column=0, row=2, sticky=tk.W)
        password = ttk.Entry(self, width=30, show="*",
                             textvariable=self.password_var)
        password.grid(column=1, row=2, sticky=tk.W)

        self.local = tk.StringVar()
        local_check = ttk.Checkbutton(
            self,
            variable=self.local,
            text='Локальный сервер')
        local_check.grid(column=1, row=3, sticky=tk.W)

        ttk.Button(self, text='Отправить',
                   command=self.onvif_process).grid(column=1, row=4)
        for widget in self.winfo_children():
            widget.grid(padx=0, pady=5)

    def progres_bar_frame(self):
        self.clearFrame()
        self.progress_frame = ttk.Frame(self)
        self.pb_label = ttk.Label(
            self.progress_frame,
            text='Пожалуйста ждите...',
            font=("Helvetica", 14))
        self.progress_frame.columnconfigure(0, weight=1)
        self.progress_frame.rowconfigure(0, weight=1)
        self.pb = ttk.Progressbar(
            self.progress_frame,
            orient=tk.HORIZONTAL,
            mode='indeterminate',
            length=280)
        self.pb_label.grid(row=0, column=1, sticky=tk.EW, padx=10, pady=10)
        self.pb.grid(row=1, column=1, sticky=tk.EW, padx=10, pady=10)
        self.progress_frame.grid(row=0, column=0, sticky=tk.NSEW)

    def start_pb(self):
        self.progres_bar_frame()
        self.pb.start(20)

    def stop_pb(self):
        self.pb.stop()
        self.clearFrame()
        self.create_result_frame()

    def clearFrame(self):
        for widget in self.winfo_children():
            widget.destroy()

    def onvif_process(self):
        self.start_pb()
        download_thread = AsyncRun(
            self.server_var.get(), self.login_var.get(), self.password_var.get(), self.local.get())
        download_thread.start()
        self.monitor(download_thread)

    def create_result_frame(self):
        ttk.Label(
            self,
            text=f'Потоков найдено:',
        ).grid(column=0, row=0, sticky=tk.W)
        ttk.Label(
            self,
            text=f'{len(self.url_list)}'
        ).grid(column=1, row=0, sticky=tk.W)

        # Метка на комбобокс
        ttk.Label(self, text='Адрес потока:').grid(
            column=0, row=1, sticky=tk.W)
        # Combobox с адресами потока
        self.stream_url = tk.StringVar()
        stream_url_box = ttk.Combobox(
            self, textvariable=self.stream_url, width=30)
        stream_url_box['values'] = self.url_list
        stream_url_box['state'] = 'readonly'
        stream_url_box.current(0)
        stream_url_box.grid(column=1, row=1, sticky=tk.NSEW)

        # Кнопка "открыть поток"
        self.stream_url_btn = ttk.Button(self, text='Открыть поток')
        self.stream_url_btn['command'] = self.play_stream
        self.stream_url_btn['state'] = 'normal'
        self.stream_url_btn.grid(column=1, row=2)

        # for widget in self.winfo_children():
        #   widget.grid(padx=0, pady=5)

    def monitor(self, thread):
        if thread.is_alive():
            self.after(100, lambda: self.monitor(thread))
        else:
            self.url_list = thread.url_list
            self.stop_pb()

    def play_stream(self):
        VideoWindow(tk.Toplevel(), "Демонстрация потока",
                    str(self.stream_url.get()))


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title('Получение ссылки на поток')
        self.geometry('400x200')
        self.resizable(0, 0)

        self.columnconfigure(0, weight=4)
        self.columnconfigure(1, weight=1)

        self.__create_widgets()

    def __create_widgets(self):
        input_frame = MainFrame(self)
        input_frame.grid(column=0, row=0)


if __name__ == "__main__":
    app = App()
    app.mainloop()
