import socket
import threading


class PortScaner:
    def __init__(self, target):
        self.target = target
        self.open_ports = []
        self.port_scaner()

    def portscan(self, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(5)
        try:
            con = s.connect((self.target, port))
            self.open_ports.append(port)
            con.close()
        except:
            pass

    def port_scaner(self):
        threads = []
        port = 500
        for _ in range(port, 600):
            t = threading.Thread(target=self.portscan, kwargs={'port': port})
            threads.append(t)
            port += 1
            t.start()
        for t in threads:
            t.join()
