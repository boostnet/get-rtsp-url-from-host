import tkinter as tk

import cv2 as cv
import PIL.Image
import PIL.ImageTk


class VideoWindow:
    def __init__(self, window, window_title, video_source=0):
        self.window = window
        self.window.title(window_title)
        self.video_source = video_source
        self.vid = MyVideoCapture(self.video_source)
        self.canvas = tk.Canvas(
            window, width=self.vid.width, height=self.vid.height)
        self.canvas.pack()
        self.delay = 30
        self.update()
        self.window.mainloop()

    def update(self):
        ret, frame = self.vid.get_frame()
        if ret:
            self.cur_frame = PIL.ImageTk.PhotoImage(
                image=PIL.Image.fromarray(frame).resize((int(self.vid.width), int(self.vid.height))))
            self.canvas.create_image(0, 0, image=self.cur_frame, anchor=tk.NW)

        self.window.after(self.delay, self.update)


class MyVideoCapture:
    def __init__(self, video_source=0):
        self.vid = cv.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Не могу открыть видеопоток!", video_source)
        self.width = int(self.vid.get(cv.CAP_PROP_FRAME_WIDTH)/2)
        self.height = int(self.vid.get(cv.CAP_PROP_FRAME_HEIGHT)/2)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                return (ret, cv.cvtColor(frame, cv.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (ret, None)

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()
